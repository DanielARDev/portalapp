﻿Shader "Portal/PortalWindow"
{
	SubShader
	{

		//render objects behind the portal
		ZWrite off
		//absolutely transparent
		ColorMask 0
		//Bidirectional behaviour
		Cull off

			Pass
			{
				Stencil{
					Ref 1
					Comp Always
					Pass replace 
				}
			}
	}
}