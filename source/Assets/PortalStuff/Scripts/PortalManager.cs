﻿#region using
using UnityEngine;
using UnityEngine.Rendering;
#endregion

/// <summary>
///  Class: PortalManager
///  UpdatedDate: 21/02/2018
/// </summary>
public class PortalManager : MonoBehaviour
{

    #region private propertys
    private readonly Transform _mainCamera;
    private readonly Material[] _materials;
    #endregion

    //bool for checking if the device is not in the same direction as it was
    bool _wasInFront;
    //bool for knowing that on the next change of state, what to set the stencil test
    bool _inOtherWorld;

    //This bool is on while device colliding, done so we ensure the shaders are being updated before render frames
    //Avoids flickering
    bool _hasCollided;

    #region builder
    public PortalManager(Transform mainCamera, Material[] materials)
    {
        _mainCamera = mainCamera;
        _materials = materials;
    }
    #endregion



    // Use this for initialization
    void Start ()
    {
        
	}

    void SetMaterials(bool fullRender)
    {
        var stencilTest = fullRender ? CompareFunction.NotEqual : CompareFunction.Equal;
        Shader.SetGlobalInt("_StencilTest", (int)stencilTest);
    }

    // Update is called once per frame
    void OnTriggerStay (Collider collider)
    {
        var camPositionInPortalSpace = transform.InverseTransformPoint(_mainCamera.transform.position * 4);

        if (camPositionInPortalSpace.y < 0.1f)
        {
            //Disable Stencil test
            foreach (var t in _materials)
            {
                t.SetInt("_StencilTest", (int)CompareFunction.NotEqual);
            }
        }
        else
        {
            //Enable stencil test
            foreach (var t in _materials)
            {
                t.SetInt("_StencilTest", (int)CompareFunction.Equal);
            }
        }

	}

    bool GetIsInFront()
    {
        var worldPos = _mainCamera.position + _mainCamera.forward * Camera.main.nearClipPlane * 4;
        var pos = transform.InverseTransformPoint(worldPos);
        return pos.z >= 0;
    }


    //This technique registeres if the device has hit the portal, flipping the bool

    void OnTriggerEnter(Collider other)
    {
        if (other.transform != _mainCamera) return;
        //Important to do this for if the user re-enters the portal from the same side
        _wasInFront = GetIsInFront();
        _hasCollided = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform != _mainCamera) return;
        _hasCollided = false;
    }


    /*If there has been a change in the relative position of the device to the portal, flip the
     *Stencil Test
     */

    void WhileCameraColliding()
    {
        if (!_hasCollided) return;

        var isInFront = GetIsInFront();

        if (isInFront && !_wasInFront || _wasInFront && !isInFront)
        {
            _inOtherWorld = !_inOtherWorld;
            SetMaterials(_inOtherWorld);
        }
        _wasInFront = isInFront;
    }


    void OnDestroy()
    {
        //ensure geometry renders in the editor
        SetMaterials(true);
    }

    void Update()
    {
        WhileCameraColliding();
    }
}
