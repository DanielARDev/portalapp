﻿#region using
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using GoogleARCore;
#endregion

/// <summary>
///  Class: GridVisualizer
///  UpdatedDate: 21/02/2018
/// </summary>

public class GridVisualizer : MonoBehaviour
{

    #region private propertys
    private static int _sPlaneCount;
    private readonly Color[] _kPlaneColors = {
            new Color(1.0f, 1.0f, 1.0f),
            new Color(0.956f, 0.262f, 0.211f),
            new Color(0.913f, 0.117f, 0.388f),
            new Color(0.611f, 0.152f, 0.654f),
            new Color(0.403f, 0.227f, 0.717f),
            new Color(0.247f, 0.317f, 0.709f),
            new Color(0.129f, 0.588f, 0.952f),
            new Color(0.011f, 0.662f, 0.956f),
            new Color(0f, 0.737f, 0.831f),
            new Color(0f, 0.588f, 0.533f),
            new Color(0.298f, 0.686f, 0.313f),
            new Color(0.545f, 0.764f, 0.290f),
            new Color(0.803f, 0.862f, 0.223f),
            new Color(1.0f, 0.921f, 0.231f),
            new Color(1.0f, 0.756f, 0.027f)
    };

    private DetectedPlane _mDetectedPlane;
    // Keep previous frame's mesh polygon to avoid mesh update every frame.
    private readonly List<Vector3> _mPreviousFrameMeshVertices = new List<Vector3>();
    private readonly List<Vector3> _mMeshVertices = new List<Vector3>();
    private Vector3 _mPlaneCenter;
    private readonly List<Color> _mMeshColors = new List<Color>();
    private readonly List<int> _mMeshIndices = new List<int>();
    private Mesh _mMesh;
    private MeshRenderer _mMeshRenderer;
    #endregion


    #region public methods

    /// <summary>
    /// The Unity Awake() method.
    /// </summary>
    public void Awake()
    {
        _mMesh = GetComponent<MeshFilter>().mesh;
        _mMeshRenderer = GetComponent<MeshRenderer>();
    }

    /// <summary>
    /// The Unity Update() method.
    /// </summary>
    public void Update()
    {
        if (_mDetectedPlane == null)  return;
        

        if (_mDetectedPlane.SubsumedBy != null)
        {
            Destroy(gameObject);
            return;
        }

        if (_mDetectedPlane.TrackingState != TrackingState.Tracking)
        {
            _mMeshRenderer.enabled = false;
            return;
        }

        _mMeshRenderer.enabled = true;

        _UpdateMeshIfNeeded();
    }

    /// <summary>
    /// Initializes the DetectedPlaneVisualizer with a DetectedPlane.
    /// </summary>
    /// <param name="plane">The plane to vizualize.</param>
    public void Initialize(DetectedPlane plane)
    {
        _mDetectedPlane = plane;
        _mMeshRenderer.material.SetColor("_GridColor", _kPlaneColors[_sPlaneCount++ % _kPlaneColors.Length]);
        _mMeshRenderer.material.SetFloat("_UvRotation", Random.Range(0.0f, 360.0f));

        Update();
    }

    #endregion


    #region private methods
    /// <summary>
    /// Update mesh with a list of Vector3 and plane's center position.
    /// </summary>
    private void _UpdateMeshIfNeeded()
    {
        _mDetectedPlane.GetBoundaryPolygon(_mMeshVertices);

        if (_AreVerticesListsEqual(_mPreviousFrameMeshVertices, _mMeshVertices)) return;
        

        _mPreviousFrameMeshVertices.Clear();
        _mPreviousFrameMeshVertices.AddRange(_mMeshVertices);

        _mPlaneCenter = _mDetectedPlane.CenterPose.position;

        var planeNormal = _mDetectedPlane.CenterPose.rotation * Vector3.up;

        _mMeshRenderer.material.SetVector("_PlaneNormal", planeNormal);

        var planePolygonCount = _mMeshVertices.Count;

        // The following code converts a polygon to a mesh with two polygons, inner
        // polygon renders with 100% opacity and fade out to outter polygon with opacity 0%, as shown below.
        // The indices shown in the diagram are used in comments below.
        // _______________     0_______________1
        // |             |      |4___________5|
        // |             |      | |         | |
        // |             | =>   | |         | |
        // |             |      | |         | |
        // |             |      |7-----------6|
        // ---------------     3---------------2
        _mMeshColors.Clear();

        // Fill transparent color to vertices 0 to 3.
        for (var i = 0; i < planePolygonCount; ++i)
        {
            _mMeshColors.Add(Color.clear);
        }

        // Feather distance 0.2 meters.
        const float featherLength = 0.2f;

        // Feather scale over the distance between plane center and vertices.
        const float featherScale = 0.2f;

        // Add vertex 4 to 7.
        for (var i = 0; i < planePolygonCount; ++i)
        {
            var v = _mMeshVertices[i];

            // Vector from plane center to current point
            var d = v - _mPlaneCenter;

            var scale = 1.0f - Mathf.Min(featherLength / d.magnitude, featherScale);
            _mMeshVertices.Add((scale * d) + _mPlaneCenter);

            _mMeshColors.Add(Color.white);
        }

        _mMeshIndices.Clear();
        var firstOuterVertex = 0;
        var firstInnerVertex = planePolygonCount;

        // Generate triangle (4, 5, 6) and (4, 6, 7).
        for (var i = 0; i < planePolygonCount - 2; ++i)
        {
            _mMeshIndices.Add(firstInnerVertex);
            _mMeshIndices.Add(firstInnerVertex + i + 1);
            _mMeshIndices.Add(firstInnerVertex + i + 2);
        }

        // Generate triangle (0, 1, 4), (4, 1, 5), (5, 1, 2), (5, 2, 6), (6, 2, 3), (6, 3, 7)
        // (7, 3, 0), (7, 0, 4)
        for (var i = 0; i < planePolygonCount; ++i)
        {
            var outerVertex1 = firstOuterVertex + i;
            var outerVertex2 = firstOuterVertex + ((i + 1) % planePolygonCount);
            var innerVertex1 = firstInnerVertex + i;
            var innerVertex2 = firstInnerVertex + ((i + 1) % planePolygonCount);

            _mMeshIndices.Add(outerVertex1);
            _mMeshIndices.Add(outerVertex2);
            _mMeshIndices.Add(innerVertex1);

            _mMeshIndices.Add(innerVertex1);
            _mMeshIndices.Add(outerVertex2);
            _mMeshIndices.Add(innerVertex2);
        }

        _mMesh.Clear();
        _mMesh.SetVertices(_mMeshVertices);
        _mMesh.SetIndices(_mMeshIndices.ToArray(), MeshTopology.Triangles, 0);
        _mMesh.SetColors(_mMeshColors);
    }

    private bool _AreVerticesListsEqual(ICollection<Vector3> firstList, IList<Vector3> secondList)
    {
        if (firstList.Count != secondList.Count) return false;

        return !firstList.Where((t, i) => t != secondList[i]).Any();
    }

    #endregion
}
