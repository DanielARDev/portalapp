﻿#region using
using UnityEngine;
using UnityEngine.SceneManagement;
#endregion

/// <summary>
///  Class: LevelChanger
///  UpdatedDate: 21/02/2018
/// </summary>

public class LevelChanger : MonoBehaviour {

    #region private propertys
    private readonly Animator _animator;
    private int _levelToLoad;
    #endregion

    #region public methods
    public LevelChanger(Animator animator)
    {
        _animator = animator;
    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(_levelToLoad);
    }

    #endregion

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            FadeToNextLevel();
        }
	}

    #region private propertys
    private void FadeToNextLevel()
    {
        FadeToLevel(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void FadeToLevel(int levelIndex)
    {
        _levelToLoad = levelIndex;
        _animator.SetTrigger("FadeOut");
    }

    #endregion

}
