﻿using UnityEngine;
using UnityEngine.Video;

/// <summary>
///  Class: VideoManager
///  CreatedDate: 08/22/2018
/// </summary>

public class VideoManager : MonoBehaviour
{
    private VideoClip[] videoClips;
    private GameObject gamePlayer;
    private const string MainCamera = "MainCamera";
    private VideoPlayer videoPlayer;
    private int videoClipIndex;

    void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
    }

    // Use this for initialization
    void Start()
    {
        gamePlayer.SetActive(false);
        videoPlayer.targetTexture.Release();
        videoPlayer.clip = videoClips[0];
    }

   

   void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.tag == MainCamera)
        {
            gamePlayer.SetActive(true);
        }

    }
    #region private propertys
    private void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == MainCamera)
        {
            gamePlayer.SetActive(false);
        }
    }
    #endregion
}
