﻿#region using
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
#endregion


/// <summary>
///  Class: ARController
///  UpdatedDate: 21/02/2018
/// </summary>

public class ARController : MonoBehaviour {

    
    #region private propertys
        private readonly List<TrackedPlane> m_NewTrackedPlane = new List<TrackedPlane>();
        private GameObject GridPrefab;
        private GameObject Portal;
        private GameObject ARCamera;

        private SceneControl sceneControl;
        bool spawned;

        private List<TrackedPlane> NewTrackedPlane
        {
            get
            {
                return m_NewTrackedPlane;
            }
        }
    #endregion


    #region methods

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        
		//Check ARCore session status
        if(Session.Status != SessionStatus.Tracking) return;
        

        //the following function will fill m_NewTrackedPlane with the planes that ARCore detected in the current frame
        Session.GetTrackables(NewTrackedPlane, TrackableQueryFilter.New);

        //Instantiate a Grid for each TrackedPlane in m_NewTrackedPlane
        foreach (var t in NewTrackedPlane)
        {
            var grid = Instantiate(GridPrefab, Vector3.zero, Quaternion.identity, transform);

            //This function will set the position of grid and modify the vertices of the attached mesh
            grid.GetComponent<GridVisualizer>().Initialize(t);
        }

        //Check if the user touches the screen
        Touch touch;
        if(Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began) return;
        

        //Let's now check if the user touched any of the tracked planes
        TrackableHit hit;
        if (!Frame.Raycast(touch.position.x, touch.position.y, TrackableHitFlags.PlaneWithinPolygon, out hit) ||
            spawned) return;

        spawned = true;
        
        //Let's now place the portal on top of the tracked plane that we touched

        //Enable the portal
        Portal.SetActive(true);

        //Create a new Anchor
        var anchor = hit.Trackable.CreateAnchor(hit.Pose);

        //Set the position of the Portal to be the same as the hit position
        Portal.transform.position = hit.Pose.position;
        Portal.transform.rotation = hit.Pose.rotation;

        //We want the Portal to face the camera
        var cameraPosition = ARCamera.transform.position;

        //The Portal only rotate around the Y axis
        cameraPosition.y = hit.Pose.position.y;

        //Rotate the Portal to face tha camera
        Portal.transform.LookAt(cameraPosition, Portal.transform.up);

        //ARCore will keep understanding the world and update the anchors accordingly hence we need to attach out Portal to the anchor
        Portal.transform.parent = anchor.transform;

        sceneControl.gameObject.SetActive(true);
    }

    #endregion
}
