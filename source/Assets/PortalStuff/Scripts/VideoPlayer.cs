﻿using UnityEngine;

/// <summary>
///  Class: VideoPlayer
///  UpdatedDate: 21/02/2018
/// </summary>
public class VieoPlayer : MonoBehaviour {
    #region private propertys
    private GameObject videoPlayer;
    private const string MainCamera = "MainCamera";
    #endregion

    // Use this for initialization


    void Start ()
    {
        videoPlayer.SetActive(false);	
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider player)
    {
        if (player.gameObject.tag == MainCamera)
        {
            videoPlayer.SetActive(true);
        }
            
	}
    #region private propertys
    private void OnTriggerExit(Collider player)
    {
        if (player.gameObject.tag == MainCamera)
        {
            videoPlayer.SetActive(false);
        }
    }
    #endregion
}
