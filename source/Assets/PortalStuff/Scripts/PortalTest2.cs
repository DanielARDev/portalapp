﻿/*
 * This Class handles flipping the global _StencilTest property when the device
 * moves throught the portal, from any direction
 */
 
#region using
using UnityEngine;
using UnityEngine.Rendering;
#endregion

/// <summary>
///  Class: PortalTest2
///  UpdatedDate: 21/02/2018
/// </summary>

public class PortalTest2 : MonoBehaviour
{
    #region private propertys
    private readonly Transform _device;
    #endregion
    //bool for checking if the device is not in the same direction as it was
    bool _wasInFront;
    //bool for knowing that on the next change of state, what to set the stencil test
    bool _inOtherWorld;

    //This bool is on while device colliding, done so we ensure the shaders are being updated before render frames
    //Avoids flickering
    bool _isColliding;

    #region Builder
    public PortalTest2(Transform device)
    {
        _device = device;
    }
    #endregion


    void Start()
    {
        //start outside other world
        SetMaterials(false);
    }

    void SetMaterials(bool fullRender)
    {
        var stencilTest = fullRender ? CompareFunction.NotEqual : CompareFunction.Equal;
        Shader.SetGlobalInt("_StencilTest", (int)stencilTest);
    }

    bool GetIsInFront()
    {
        var worldPos = _device.position + _device.forward * Camera.main.nearClipPlane;
        var pos = transform.InverseTransformPoint(worldPos);
        return pos.z >= 0;
    }


    //This technique registeres if the device has hit the portal, flipping the bool

    void OnTriggerEnter(Collider other)
    {
        if (other.transform != _device) return;
        //Important to do this for if the user re-enters the portal from the same side
        _wasInFront = GetIsInFront();
        _isColliding = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform != _device) return;
        _isColliding = false;
    }


    /*If there has been a change in the relative position of the device to the portal, flip the
     *Stencil Test
     */

    void WhileCameraColliding()
    {
        if (!_isColliding) return;

        var isInFront = GetIsInFront();

        if ((isInFront && !_wasInFront) || (_wasInFront && !isInFront))
        {
            _inOtherWorld = !_inOtherWorld;
            SetMaterials(_inOtherWorld);
        }
        _wasInFront = isInFront;
    }

    void OnDestroy()
    {
        //ensure geometry renders in the editor
        SetMaterials(true);
    }


    void Update()
    {
        WhileCameraColliding();
    }
}
