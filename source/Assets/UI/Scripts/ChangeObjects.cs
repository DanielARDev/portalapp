﻿#region using
using UnityEngine;
#endregion

/// <summary>
///  Class: ChangeObjects
///  UpdatedDate: 08/22/2018
/// </summary>
public class ChangeObjects : MonoBehaviour {

    #region private propertys
    int _currentOrnamentId = 0;
    private readonly GameObject[] _ornaments;
    private readonly GameObject _nextButton;
    private readonly GameObject _previousButton;
    #endregion

    #region Builder
    public ChangeObjects(GameObject[] ornaments,
        GameObject nextButton,
        GameObject previousButton)
    {
        this._ornaments = ornaments;
        this._nextButton = nextButton;
        this._previousButton = previousButton;
    }
    #endregion



    // Use this for initialization
    void Start ()
    {
        _previousButton.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    #region private methods
    private void EnableOrnament(int newId, int previousId)
    {
        _ornaments[previousId].SetActive(false);
        _ornaments[newId].SetActive(true);
    }
    #endregion


    #region public methods
    public void Next()
    {

        var nextS = "next";

        switch (_currentOrnamentId)
        {
            case 0:
                Debug.Log(nextS + LevelNext.One);
                _currentOrnamentId = LevelNext.One;
                EnableOrnament(LevelNext.One, LevelNext.Zero);
                _previousButton.SetActive(true);
                break;
            case 1:
                Debug.Log(nextS + LevelNext.Two);
                _currentOrnamentId = LevelNext.Two;
                EnableOrnament(LevelNext.Two, LevelNext.One);
                break;
            case 2:
                Debug.Log(nextS + LevelNext.Three);
                _currentOrnamentId = LevelNext.Three;
                EnableOrnament(LevelNext.Three, LevelNext.Two);
                break;
            case 3:
                Debug.Log(nextS + LevelNext.Four);
                _currentOrnamentId = LevelNext.Four;
                EnableOrnament(LevelNext.Four, LevelNext.Three);
                break;
            case 4:
                Debug.Log(nextS + LevelNext.Five);
                _currentOrnamentId = LevelNext.Five;
                EnableOrnament(LevelNext.Five, LevelNext.Four);
                _nextButton.SetActive(false);
                break;
            case 5:
                Debug.Log(nextS + LevelNext.Six);
                _currentOrnamentId = LevelNext.Six;
                break;
        }
    }

    public void Previous()
    {
        switch(_currentOrnamentId)
        {
            case 0:
                Debug.Log(LevelPrevious.Six);
                _currentOrnamentId = LevelPrevious.Six;
                EnableOrnament(LevelPrevious.Six, LevelPrevious.Zero);
                break;
            case 1:
                Debug.Log(LevelPrevious.Five);
                _previousButton.SetActive(false);
                _currentOrnamentId = LevelPrevious.Zero;
                EnableOrnament(LevelPrevious.Zero, LevelPrevious.One);
                break;
            case 2:
                Debug.Log(LevelPrevious.Four);
                _currentOrnamentId = LevelPrevious.One;
                EnableOrnament(LevelPrevious.One, LevelPrevious.Two);
                break;
            case 3:
                Debug.Log(LevelPrevious.Three);
                _currentOrnamentId = LevelNext.Two;
                EnableOrnament(LevelPrevious.Two, LevelPrevious.Three);
                break;
            case 4:
                Debug.Log(LevelPrevious.Two);
                _currentOrnamentId = LevelPrevious.Three;
                EnableOrnament(LevelPrevious.Three, LevelPrevious.Four);
                break;
            case 5:
                Debug.Log(LevelPrevious.One);
                _nextButton.SetActive(true);
                _currentOrnamentId = LevelPrevious.Four;
                EnableOrnament(LevelPrevious.Four, LevelPrevious.Five);
                break;
            case 6:
                Debug.Log(LevelPrevious.Zero);
                _currentOrnamentId = LevelPrevious.Five;
                EnableOrnament(LevelPrevious.Five, LevelPrevious.Six);
                break;
        }
    }

    #endregion
}


#region internal class

internal static class LevelNext
{
    public const int Zero = 0;
    public const int One = 1;
    public const int Two = 2;
    public const int Three = 3;
    public const int Four = 4;
    public const int Five = 5;
    public const int Six = 6;

}


internal static class LevelPrevious
{
    public const int Zero = 0;
    public const int One = 1;
    public const int Two = 2;
    public const int Three = 3;
    public const int Four = 4;
    public const int Five = 5;
    public const int Six = 6;

}

#endregion

