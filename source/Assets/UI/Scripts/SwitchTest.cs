﻿#region using
using UnityEngine;
#endregion
public class SwitchTest : MonoBehaviour
{

    #region private propertys
    private readonly GameObject _object1;
    private readonly GameObject _object2;
    private readonly GameObject _object3;
    #endregion

    #region Builder
    public SwitchTest(GameObject object1, GameObject object2, GameObject object3)
    {
        _object1 = object1;
        _object2 = object2;
        _object3 = object3;
    }
    #endregion

    #region public methods

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void Object1()
    {
        _object1.SetActive(true);
        _object2.SetActive(false);
        _object3.SetActive(false);
    }

    public void Object2()
    {
        _object1.SetActive(false);
        _object2.SetActive(true);
        _object3.SetActive(false);
    }

    public void Object3()
    {
        _object1.SetActive(false);
        _object2.SetActive(false);
        _object3.SetActive(true);
    }

    #endregion
}
