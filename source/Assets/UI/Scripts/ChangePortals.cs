﻿#region using
using UnityEngine;
#endregion

/// <summary>
///  Class: ChangePortals
///  UpdatedDate: 08/22/2018
/// </summary>
public class ChangePortals : MonoBehaviour {

    #region private propertys
    private readonly GameObject[] _prefabs;
    private int _currentNomber;
    private GameObject _currentInstance;
    private readonly GUIStyle _guiStyleHeader = new GUIStyle();
    float _dpiScale;
    #endregion

    #region Builder
    public ChangePortals(GameObject[] prefabs)
    {
        _prefabs = prefabs;
    }
    #endregion

    void Start()
    {
        if (Screen.dpi < 1) _dpiScale = 1;
        if (Screen.dpi < 200) _dpiScale = 1;
        else _dpiScale = Screen.dpi / 200f;
        _guiStyleHeader.fontSize = (int)(15f * _dpiScale);
        _guiStyleHeader.normal.textColor = new Color(0.15f, 0.15f, 0.15f);
        _currentInstance = Instantiate(_prefabs[_currentNomber], transform.position, new Quaternion());
    }

    void ChangeCurrent(int delta)
    {
        _currentNomber += delta;
        if (_currentNomber > _prefabs.Length - 1)
            _currentNomber = 0;
        else if (_currentNomber < 0)
            _currentNomber = _prefabs.Length - 1;
        if (_currentInstance != null) Destroy(_currentInstance);
        _currentInstance = Instantiate(_prefabs[_currentNomber], transform.position, new Quaternion());
    }


    #region private methods
    private void OnGUI()
    {
        if (GUI.Button(new Rect(10 * _dpiScale, 15 * _dpiScale, 135 * _dpiScale, 37 * _dpiScale), "PREVIOUS"))
        {
            ChangeCurrent(-1);
        }
        if (GUI.Button(new Rect(160 * _dpiScale, 15 * _dpiScale, 135 * _dpiScale, 37 * _dpiScale), "NEXT"))
        {
            ChangeCurrent(+1);
        }
    }
    #endregion
}
