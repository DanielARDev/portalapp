﻿#region using
    using UnityEngine;
    using UnityEngine.SceneManagement;
#endregion
public class SceneControl : MonoBehaviour
{

    int _sceneIndex;

    // Use this for initialization
    void Start()
    {
        _sceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
    }
    #region public methods
    public void LoadNextLevel()
    {
        SceneManager.LoadScene(_sceneIndex + 1);
    }
    public void LoadPreviousLevel()
    {
        SceneManager.LoadScene(_sceneIndex - 1);
    }

    #endregion
}
